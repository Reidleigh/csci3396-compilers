#include "scanner.h"

class Parser {
    public:
    Scanner s;
    Production p;
    int epsilon, eof, S; // S is the starting nt, poorly calculated.
    std::list<int> terminals, nonTerminals;
    std::list<Production> productions;
    std::list<Token>::iterator it;
    std::vector<std::list<int>> first, follow,next;
    std::map<std::tuple<int,int>,int> nextMap;
    
    // Stuff for parsing
    bool parse(FILE * f);
    bool Grammar();
    bool ProductionList();
    bool ProductionListPrime();
    bool ProductionSet();
    bool ProductionSetPrime();
    bool RightHandSide();
    bool SymbolList();
    bool SymbolListPrime();
    void add();

    // Stuff for First, Follow, and Next
    void computeSets();
    void computeFirst();
    void computeFollow();
    void computeNext();
    bool addSet(std::list<int> * left, std::list<int> right, int ex = -5);
    std::string f(int i); //for converting to strings

    // Print methods
    void print();
    void printProduction(Production pr);
    void printFirst();
    void printFollow();
    void printNext();
    void printNextMap();
    void printYAML();

    //makeLL1()
    void makeLL1(FILE * f);

};

void testParser();
