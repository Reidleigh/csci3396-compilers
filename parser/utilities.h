#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <iomanip>
#include <iterator>
#include <string>
#include <list>
#include <vector>
#include <map>
#include <tuple>
#include <algorithm>

bool end(FILE * f);
void skipWhiteSpace(FILE * f);
void verify(bool ex, const char * msg);
FILE* openFile(const char * s);
void printHelp();
