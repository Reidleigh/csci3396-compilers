#include "scanner.h"

bool Symbols::contains(int i) {   
    return intToSymbol.find(i)!=intToSymbol.end();
}
             
bool Symbols::contains(std::string s) {
    return symbolToInt.find(s)!=symbolToInt.end(); 
}   
    
int Symbols::getInt(std::string s) {
    return symbolToInt.find(s)->second; 
}   
    
std::string Symbols::getSym(int i) {
    return intToSymbol.find(i)->second;
}

int Symbols::add(std::string s) {
    if (contains(s))
        return getInt(s);
    else {
        if (s=="epsilon"||s=="Epsilon"||s=="EPSILON") {
            intToSymbol[idNo++] = "epsilon";
            symbolToInt["epsilon"] = idNo-1;
            epsilon = idNo-1;
        }
        else {
            intToSymbol[idNo++] = s;
            symbolToInt[s] = idNo-1;
        }
        return (idNo-1);
    }
}

void Symbols::print() {
    printf("\nPrinting Symbols Maps\n");
    printf("Printing intToSymbol\n\n");
    for (auto i=intToSymbol.begin();i!=intToSymbol.end();i++) {
        std::cout << std::setw(8)  << i->first << " " << std::setw(5)  << i->second << std::endl;
    }
    printf("Printing symbolToInt\n\n");
    for (auto i=symbolToInt.begin();i!=symbolToInt.end();i++) {
        std::cout << std::setw(8) << i->first << " " << std::setw(5) << i->second << std::endl;
    }
}

void Scanner::scanTokens(FILE * f) {
    while (!end(f)) {
        scanToken(f);
    }
    if (sym.epsilon==-1) {
        sym.intToSymbol[sym.idNo++] = "epsilon";
        sym.symbolToInt["epsilon"] = sym.idNo-1;
        sym.epsilon = sym.idNo-1;
    }
    sym.intToSymbol[sym.idNo++] = "eof";
    sym.symbolToInt["eof"] = sym.idNo-1;
    sym.eof = sym.idNo-1;
}

void Scanner::print() {
    printf("\nPrinting sym\n\n");
    sym.print();
    printf("\nPrinting tokens\n\n");
    for (auto i=tokens.begin();i!=tokens.end();i++) {
        std::cout << "type: " << i->print() << "\nint: " << i->no << "\nname: ";
        std::cout << sym.getSym(i->no) << "\n" << std::endl;
    }
}

void Scanner::scanToken(FILE * f) {
    std::string s;
    skipWhiteSpace(f);
    char c = getc(f);
    if (c==';') {
        tokens.push_back(Token(-1,SEMICOLON));
        skipWhiteSpace(f);
    }
    else if (c==':') {
        tokens.push_back(Token(-2,DERIVES));
        skipWhiteSpace(f);
    }
    else if (c=='|') {
        tokens.push_back(Token(-3,ALSODERIVES));
        skipWhiteSpace(f);
    }
    else if (isalnum(c)!=0) {
        while (isalnum(c)!=0) {
            s.push_back(c);
            c = getc(f);
        }
        ungetc(c,f);
        if (s=="epsilon"||s=="Epsilon"||s=="EPSILON") {
            tokens.push_back(Token(sym.add(s),EPSILON));
            skipWhiteSpace(f);
        }
        else {
            tokens.push_back(Token(sym.add(s),SYMBOL));
            skipWhiteSpace(f);
        }
    }
    else {
        verify(false,"Error: Invalid Character\n");
    }
}

void Production::print() {
    std::cout << std::setw(7) << nt << " : ";
    for (auto i=rhs.begin();i!=rhs.end();i++) {
        std::cout << (*i) << " ";
    }
    std::cout << "\n        ;" << std::endl;
}

void testScanner() {
    printf("BEGIN TEST SCANNER\n");
    FILE * f;
    f = fopen("test.tokens","r");
    if (f==NULL) {
    printf("F IS NULL\n");
    exit(-1);
    }
    Scanner s;
    s.scanTokens(f);
    s.print();
}
