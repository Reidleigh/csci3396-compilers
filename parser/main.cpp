#include "parser.h"

int main (int argc, char * argv[]) {
    bool s = false;
    bool r = false;
    bool t = false;
    for (int i=1;i<argc;i++) {
        if (strcmp(argv[i],"-h")==0) {
            printHelp();
            return 0;
        }
        if (strcmp(argv[i],"-t")==0) {
            t = true;
        }
        if (strcmp(argv[i],"-s")==0) {
            s = true;
        }
        if (strcmp(argv[i],"-r")==0) {
            r = true;
        }
    }

    FILE * f;
    f = openFile(argv[argc-1]);
    Parser p;   
    p.parse(f);
    p.computeSets();

    if (r) {
        //transform to LL1 Grammar
    }

    if (s) {
        p.print();
    }

    if (t) {
        p.printYAML();
    }

    return 0;
}
