#include "utilities.h"

enum termType { SEMICOLON, DERIVES, ALSODERIVES, EPSILON, SYMBOL };

class Token {
    public:
    int no;
    termType type;
    Token(int i, termType t) {
        no = i;
        type = t;
    };
    std::string print() {
        switch(type) {
        case SEMICOLON: return "SEMICOLON";
        case DERIVES: return "DERIVES";
        case ALSODERIVES: return "ALSODERIVES";
        case EPSILON: return "EPSILON";
        case SYMBOL: return "SYMBOL";
        }
    };    
};       
        
class Symbols {
    public: 
    std::map<int,std::string> intToSymbol;
    std::map<std::string,int> symbolToInt;
    bool contains(int i); 
    bool contains(std::string s);
    int idNo;
    int epsilon;
    int eof;
    
    Symbols() {
    idNo = 0;
    epsilon = -1;
    intToSymbol[-1] = ";";
    intToSymbol[-2] = ":";
    intToSymbol[-3] = "|";
    }
    int getInt(std::string s);
    std::string getSym(int i);

    int add(std::string s);
    void print();
};

class Scanner {
    public:
    Symbols sym;
    std::list<Token> tokens;
    void scanToken(FILE * f);
    
    void scanTokens(FILE * f);
    void print();
};

class Production {
    public:
    int nt;
    std::list<int> rhs;

    Production(int n) {
        nt = n;
    };
    Production() = default;
    ~Production() = default;
    void print();

};

void testScanner();
