#include "utilities.h"

bool end(FILE * f) {
    char c = getc(f);
    if (c == EOF)
        return true;
    else {
        ungetc(c,f);
        return false;
    }
}

void skipWhiteSpace(FILE * f) {
    bool flag = false;
    char c;
    while (!flag) {
        c = getc(f);
        if (isspace(c)==0) {
            if (c=='/') {
                c = getc(f);
                if (c=='/') {
                    while (c != '\n')
                        c = getc(f);
                } else {
                    printf("c = '%c'\n",c);
                    fprintf(stderr, "Invalid comment!\n"); 
                }
            } else {
                flag = true;
            } 
        }
    }
    ungetc(c,f);
}

void verify(bool ex, const char * msg) {
    if (!ex) {
        fprintf(stderr,"%s",msg);
        exit(-1);
    }
}

FILE* openFile(const char * s) {
    FILE * f;
    f = fopen(s,"r");
    verify((f!=NULL),"Error: unable to open file\n");
    return f;
}

void printHelp() {
    printf("Usage: llgen [options] [filename] \n");
    printf("  -h            Display usage message.\n");
    printf("  -t            Print the LL(1) table in YAML format.\n");
    printf("  -s            Print the first, follow, and next sets to stdout\n");
    printf("  -r            Remove left recursion and factor common prefixes in non-LL(1 grammar\n");
}
