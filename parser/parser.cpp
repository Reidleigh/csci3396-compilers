#include "parser.h"

bool Parser::parse(FILE * f) {
    s.scanTokens(f);
    it = s.tokens.begin();
    bool grammar=Grammar();

    // remove terminals from list of nonTerminals
    for (auto i=nonTerminals.begin();i!=nonTerminals.end();i++) {
        terminals.remove(*i);
    }

    return grammar;
}

bool Parser::Grammar() {
    return ProductionList();
}

bool Parser::ProductionList() {
    if (ProductionSet()) {
        if (it->type==SEMICOLON) {
            add();
            it++;
            return ProductionListPrime();
        }
    }
    return false;
}

bool Parser::ProductionListPrime() {
    if (it==s.tokens.end()) {
        return true;
    }
    else if (ProductionSet()) {
        if (it->type==SEMICOLON) {
            add();
            it++;
            return ProductionListPrime();
        }
        else return false;
    }
    else
        return false;
}

bool Parser::ProductionSet() {
    if (it->type!=SYMBOL) {
        return false;
    }
    else {
        p = Production(it->no);
        it++;
        if (it->type!=DERIVES) {
            return false;
        }
        else {
            it++;
            if (RightHandSide()) {
                return ProductionSetPrime();
            }
            else return false;
        }
    }
}

bool Parser::ProductionSetPrime() {
    if (it->type==SEMICOLON) {
        return true;
    }
    else {
        if (it->type==ALSODERIVES) {
            int n = p.nt;
            add();
            p.nt = n;
            it++;
            if (RightHandSide())
                return ProductionSetPrime();
            else
                return false;
        }
        else
            return false;
    }
}

bool Parser::RightHandSide() {
    return SymbolList();
}


bool Parser::SymbolList() {
    if ((it->type!=SYMBOL) && (it->type!=EPSILON)) {
        return false;
    }
    else {
        p.rhs.push_back(it->no);
        it++;
        return SymbolListPrime();
    }
}

bool Parser::SymbolListPrime() {
    if ((it->type==ALSODERIVES)||(it->type==SEMICOLON)) {
        return true;
    }
    else {
        if (it->type!=SYMBOL) {
            return false;
        }
        else {
            p.rhs.push_back(it->no);
            it++;
            return SymbolListPrime();
        }
    }
}

void Parser::add() {
    productions.push_back(p);
    if (std::find(nonTerminals.begin(), nonTerminals.end(), p.nt)==nonTerminals.end()) {
        nonTerminals.push_back(p.nt);
    }
    for (auto i=p.rhs.begin();i!=p.rhs.end();i++) {
        if (std::find(terminals.begin(),terminals.end(),*i)==terminals.end()) terminals.push_back(*i);
    }
    p = Production();
}

//Stuff for First, Follow, and Next

void Parser::computeSets() {
    epsilon = s.sym.epsilon;
    eof = s.sym.eof;

    first.resize(s.sym.idNo);
    computeFirst();

    follow.resize(s.sym.idNo);
    computeFollow();

    next.resize(productions.size());
    computeNext();
}

void Parser::computeFirst() {
    bool firstChanging = true;
    for (auto i=terminals.begin();i!=terminals.end();i++) {
        first[*i].push_back(*i);
    }        
    if (*first[epsilon].begin()!=epsilon) {
        first[epsilon].push_back(epsilon);
    }
    first[eof].push_back(eof);

    while (firstChanging) {
        firstChanging = false;
        for (auto P_i = productions.begin(); P_i != productions.end(); P_i++) {
            std::list<int> RHS;
            std::list<int>::iterator B_i = P_i->rhs.begin();
            addSet(&RHS, first[*B_i], epsilon);
            int i = 1;
            int k = P_i->rhs.size();
            if (k==1) {
                if (std::find(nonTerminals.begin(),nonTerminals.end(),*(P_i->rhs.begin()))!=nonTerminals.end()) {
                    S = P_i->nt;
                }
            }
            while ((i<k) && (std::find(first[*B_i].begin(),first[*B_i].end(),epsilon)!=first[*B_i].end())) {
                i++;
                B_i++;
                addSet(&RHS, first[*B_i], epsilon);
            }
            if ((i==k) && (std::find(first[*B_i].begin(),first[*B_i].end(),epsilon)!=first[*B_i].end())) {
                    std::list<int> ep;
                    ep.push_back(epsilon);
                    addSet(&RHS,ep);
            }
            if (addSet(&first[P_i->nt],RHS))
                firstChanging = true;
        }
    }
}

void Parser::computeFollow() {
    follow[S].push_back(eof);
    bool changing = true;
    while (changing) {
        changing = false;
        for (auto P_i=productions.begin();P_i!=productions.end();P_i++) {
            std::list<int> trailer;
            addSet(&trailer,follow[P_i->nt]);
            for (auto i=P_i->rhs.rbegin();i!=P_i->rhs.rend();i++) {
                if (std::find(nonTerminals.begin(),nonTerminals.end(),*i)!=nonTerminals.end()) {
                    if (addSet(&(follow[*i]),trailer)) changing = true;
                    if (std::find(first[*i].begin(),first[*i].end(),epsilon)!=first[*i].end()) {
                        addSet(&trailer,first[*i],epsilon);
                    }
                    else
                        trailer = first[*i];
                }
                else
                    trailer = first[*i];
            }
        }
    }
}

void Parser::computeNext() {
    int p=0;
    for (auto i=productions.begin();i!=productions.end();i++) {
        bool epsilonInAll = true;
        for (auto r=i->rhs.begin();r!=i->rhs.end();r++) {
            if (std::find(first[*r].begin(),first[*r].end(),epsilon)!=first[*r].end()) {
                addSet(&next[p],first[*r]);
            }
            else {
                epsilonInAll = false;
                addSet(&next[p],first[*r]);
                next[p].remove(epsilon);
                while (r!=i->rhs.end()) r++;
                r--;
            }
        }
        if (epsilonInAll) addSet(&next[p],follow[i->nt]);
        p++;
    }
    // here's where you need to do the nextMap stuff
    p=0;
    for (auto i=productions.begin();i!=productions.end();i++) {
            for (auto n=next[p].begin();n!=next[p].end();n++) {
                if (nextMap.find(std::make_tuple(i->nt,*n))==nextMap.end()) {
                    nextMap[std::make_tuple(i->nt,*n)] = p;
                }
                else {
                    fprintf(stderr,"Error: Grammar is not LL(1)!\n");
                    exit(-1);
                }
            }
        p++;
    }
}

/*
void Parser::computeFirst() {
    bool firstChanging = true;
    for (auto i=terminals.begin();i!=terminals.end();i++) {
        first[*i].push_back(*i);
    }        
    if (*first[epsilon].begin()!=epsilon) {
        first[epsilon].push_back(epsilon);
    }
    first[eof].push_back(eof);

    while (firstChanging) {
        firstChanging = false;
        for (auto P_i = productions.begin(); P_i != productions.end(); P_i++) {
            std::list<int> RHS;
            std::list<int>::iterator B_i = P_i->rhs.begin();
            addSet(&RHS, first[*B_i], epsilon);
            int i = 1;
            int k = P_i->rhs.size();
            if (k==1) {
                if (std::find(nonTerminals.begin(),nonTerminals.end(),*(P_i->rhs.begin()))!=nonTerminals.end()) {
                    S = P_i->nt;
                }
            }
            while ((i<k) && (std::find(first[*B_i].begin(),first[*B_i].end(),epsilon)!=first[*B_i].end())) {
                i++;
                B_i++;
                addSet(&RHS, first[*B_i], epsilon);
            }
            if ((i==k) && (std::find(first[*B_i].begin(),first[*B_i].end(),epsilon)!=first[*B_i].end())) {
                    std::list<int> ep;
                    ep.push_back(epsilon);
                    addSet(&RHS,ep);
            }
            if (addSet(&first[P_i->nt],RHS))
                firstChanging = true;
        }
    }
}

void Parser::computeFollow() {
    follow[S].push_back(eof);
    bool changing = true;
    while (changing) {
        changing = false;
        for (auto P_i=productions.begin();P_i!=productions.end();P_i++) {
            std::list<int> trailer;
            addSet(&trailer,follow[P_i->nt]);
            for (auto i=P_i->rhs.rbegin();i!=P_i->rhs.rend();i++) {
                if (std::find(nonTerminals.begin(),nonTerminals.end(),*i)!=nonTerminals.end()) {
                    if (addSet(&(follow[*i]),trailer)) changing = true;
                    if (std::find(first[*i].begin(),first[*i].end(),epsilon)!=first[*i].end()) {
                        addSet(&trailer,first[*i],epsilon);
                    }
                    else
                        trailer = first[*i];
                }
                else
                    trailer = first[*i];
            }
        }
    }
}

void Parser::computeNext() {
    int p=0;
    for (auto i=productions.begin();i!=productions.end();i++) {
        bool epsilonInAll = true;
        for (auto r=i->rhs.begin();r!=i->rhs.end();r++) {
            if (std::find(first[*r].begin(),first[*r].end(),epsilon)!=first[*r].end()) {
                addSet(&next[p],first[*r]);
            }
            else {
                epsilonInAll = false;
                addSet(&next[p],first[*r]);
                next[p].remove(epsilon);
                while (r!=i->rhs.end()) r++;
                r--;
            }
        }
        if (epsilonInAll) addSet(&next[p],follow[i->nt]);
        p++;
    }
    // here's where you need to do the nextMap stuff
    p=0;
    for (auto i=productions.begin();i!=productions.end();i++) {
            for (auto n=next[p].begin();n!=next[p].end();n++) {
                if (nextMap.find(std::make_tuple(i->nt,*n))==nextMap.end()) {
                    nextMap[std::make_tuple(i->nt,*n)] = p;
                }
                else {
                    fprintf(stderr,"Error: Grammar is not LL(1)!\n");
                    exit(-1);
                }
            }
        p++;
    }
}
*/

bool Parser::addSet(std::list<int> * left, std::list<int> right, int ex) {
    bool flag = false;
    if (ex==-5) {
        for (auto i=right.begin();i!=right.end();i++) {
            if (std::find(left->begin(),left->end(),*i)==left->end()) {
                left->push_back(*i);
                flag = true;
            }
        }
    }
    else {
        for (auto i=right.begin();i!=right.end();i++) {
            if (*i!=ex) {
                if (std::find(left->begin(),left->end(),*i)==left->end()) {
                    left->push_back(*i);
                    flag = true;
                }
            }
        }
    }
    return flag;
}

std::string Parser::f(int i) {
    return s.sym.getSym(i);
}

//Print methods

void Parser::print() {
    std::cout << "\nProductions:" << std::endl;
    for (auto i=productions.begin();i!=productions.end();i++) {
        printProduction(*i);
    }
    std::cout << std::endl;
    printFirst();
    std::cout << std::endl;
    printFollow();
    std::cout << std::endl;
    printNext();
    std::cout << std::endl;
    printNextMap();
    std::cout << std::endl;
}

void Parser::printProduction(Production pr) {
    std::cout << std::setw(7) << f(pr.nt) << " : ";
    for (auto i=pr.rhs.begin();i!=pr.rhs.end();i++) {
        std::cout << f(*i) << " ";
    }
    std::cout << "\n        ;" << std::endl;
}

void Parser::printFirst() {
    int i_int=0;
    std::cout << "First Sets:" << std::endl;
    for (auto i=first.begin();i!=first.end();i++) {
        std::cout << std::setw(10) << f(i_int) << ": ";
        i_int++;
        for (auto iter=i->begin();iter!=i->end();iter++) {
            std::cout << f(*iter) << " ";
        }
        std::cout << std::endl;
    }
}

void Parser::printFollow() {
    std::cout << "Follow Sets:" << std::endl;
    for (auto i=nonTerminals.begin();i!=nonTerminals.end();i++) {
        std::cout << std::setw(10) << f(*i) << ": ";
        for (auto iter=follow[*i].begin();iter!=follow[*i].end();iter++) {
            std::cout << f(*iter) << " ";
        }
        std::cout << std::endl;
    }
}

void Parser::printNext() {
    int p = 0;
    std::cout << "Next Sets:" << std::endl;
    for (auto i=productions.begin();i!=productions.end();i++) {
        std::cout << std::setw(10) << p << " " << f(i->nt) << ": ";
        for (auto iter=next[p].begin();iter!=next[p].end();iter++) {
            std::cout << f(*iter) << " ";
        }
        std::cout << std::endl;
        p++;
    }
}

void Parser::printNextMap() {
    int greatest = 0;
    int ntgreatest = 0;
    std::cout << "Next Sets in Map Format:" << std::endl;
    for (auto i=terminals.begin();i!=terminals.end();i++) {
        if (f(*i).size()>greatest)
            greatest = f(*i).size();
    }
    for (auto i=nonTerminals.begin();i!=nonTerminals.end();i++) {
        if (f(*i).size()>ntgreatest)
            ntgreatest = f(*i).size();
    }
    std::cout << "| ";
    for (int i=0;i<=ntgreatest;i++) {
        std::cout << " ";
    }
    std::cout << "| ";
    for (auto i=terminals.begin();i!=terminals.end();i++) {
        std::cout << std::setw(greatest) << f(*i) << " | ";
    }
    std::cout << std::endl;
    for (auto i=nonTerminals.begin();i!=nonTerminals.end();i++) {
        std::cout << "| " << std::setw(ntgreatest) << f(*i) << " | ";
        for (auto t=terminals.begin();t!=terminals.end();t++) {
            if (nextMap.find(std::make_tuple(*i,*t))==nextMap.end()) {
                std::cout << std::setw(greatest) << "--" << " | ";
            }
            else
                std::cout << std::setw(greatest) << nextMap.find(std::make_tuple(*i,*t))->second << " | ";
        }
        std::cout << std::endl;
    }
}

void Parser::printYAML() {
    auto i=terminals.begin();
    std::cout << "terminals: [" << f(*i);
    for (i++;i!=terminals.end();i++) {
        std::cout << ", " << f(*i);
    }
    std::cout << "]" << std::endl;
    i=nonTerminals.begin();
    std::cout << "non-terminals: [" << f(*i);
    for (i++;i!=nonTerminals.end();i++) {
        std::cout << ", " << f(*i);
    }
    std::cout << "]" << std::endl;
    std::cout << "eof-marker: <EOF>" << std::endl;
    std::cout << "error-marker: --" << std::endl;
    std::cout << "start-symbol: " << f(S) << std::endl;
    std::cout << "productions:" << std::endl;
    int n = 0;
    for (auto p=productions.begin();p!=productions.end();p++) {
        auto x=p->rhs.begin();
        std::cout << " " << n << ": {" << f(p->nt) << ": [";
        if (*x != epsilon)
            std::cout << f(*x);
        for (x++;x!=p->rhs.end();x++) {
            if (*x!=epsilon)
            std::cout << ", " << f(*x);
        }
        std::cout << "]}" << std::endl;
        n++;
    }
    std::cout << "table:" << std::endl;
    for (auto nt=nonTerminals.begin();nt!=nonTerminals.end();nt++) {
        std::cout << " " << f(*nt) << ": {";
        for (auto t=terminals.begin();t!=terminals.end();t++) {
            if (nextMap.find(std::make_tuple(*nt,*t))==nextMap.end()) {
                std::cout << f(*t) << ": --, ";
            }
            else
                std::cout << f(*t) << ": " << nextMap.find(std::make_tuple(*nt,*t))->second << ", ";
        }
        if (nextMap.find(std::make_tuple(*nt,eof))==nextMap.end())
            std::cout << "<EOF>: --";
        else
            std::cout << "<EOF>: " << nextMap.find(std::make_tuple(*nt,eof))->second;
        std::cout << "}" << std::endl;
    }
}

void testParser() {
    FILE * f;
    f = fopen("test.gram","r");

    printf("\nTESTING PARSER\n");
    Parser p;
    if (p.parse(f)) printf("PARSING SUCCESSFUL\n");
    else printf("PARSING UNSUCCESSFUL\n");
    p.print();

    
    printf("\nEND TESTING\n");    
}

void Parser::makeLL1(FILE * f) {
    parse(f);
    
}

























