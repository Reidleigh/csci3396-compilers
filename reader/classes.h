#include "utilities.h"



//      ENUMS



enum tokenCategory {INST, REG, CONST, COMMA, ARROW};
enum opCode {load, loadI, store, add, sub, mult, lshift, rshift, output, nop};
enum opType {Reg, Const, NA}; 

const char* getTokenCategory(enum tokenCategory cat);
const char* getOpCode(enum opCode op);



//      CLASSES



class Token {
    public:
    enum tokenCategory category;
    enum opCode opcode;
    int value;
};

class Operand {
    public:
    enum opType type;
    int SR, VR, PR, NU;

    void print();
    void prettyPrint();
    void testPrint();
};

class Instruction {
    public:
    enum opCode opcode;
    class Operand OP1;
    class Operand OP2;
    class Operand OP3;

    void print();
    void prettyPrint();
    void testPrint(int i);
};

class Block {
    public:
    int VRName;
    std::vector<Instruction> ib;
    std::list<int> PRs;
    std::vector<int> SRtoVR;
    std::vector<int> LastUse;
    std::vector<int> VRtoPR;
    std::vector<int> VRtoLU;

    int maxSR();
    int maxVR();
    void printPRs();
    void printSRtoVR();
    void printLastUse();
    void printVRtoPR();
};



//      STANDARD PRINT FUNCTIONS
    


void printToken(Token t);
void printBlock(Block b);
void printCode(Block b);
void testPrint(Block b);



//      PRINT HELP FUNCTIONS



void printHelpReader();
void printHelpAllocator();
