#include "parser.h"

int main (int argc, char * argv[])
{
    FILE * f;
    
    if (argc == 1) {
        printf("Error: No file argument\n"); 
        return 0;
    }
    
    if (strcmp(argv[1],"-h") == 0) {
        printHelpReader();
        return 0;
    }
    
    f = fopen(argv[(argc-1)],"r"); 
    check((f!=NULL),"Error: Unable to open file.\n");

    if (strcmp(argv[1],"-t") == 0) {
        while(!end(f)) {
            Token t = scanToken(f);
            printToken(t);
        }
        return 0;
    }

    Block block;
    block = parseBlock(f);

    if (strcmp(argv[1],"-p") == 0) {
        printCode(block);
        return 0;
    }

    printBlock(block);
    return 0;
}
