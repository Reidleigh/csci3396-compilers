#include "classes.h"



//      OPERAND FUNCTIONS



const char* getTokenCategory(enum tokenCategory cat) {
    switch(cat) {
        case INST: return "INST";
        case REG: return "REG";
        case CONST: return "CONST";
        case COMMA: return "COMMA";
        case ARROW: return "ARROW";
    }
}

const char* getOpCode(enum opCode op) {
    switch(op) {
        case load: return "load";
        case loadI: return "loadI";
        case store: return "store";
        case add: return "add";
        case sub: return "sub";
        case mult: return "mult";
        case lshift: return "lshift";
        case rshift: return "rshift";
        case output: return "output";
        case nop: return "nop";
    }
} 

void Operand::print() {
    switch (type) {
        case Reg: {
            printf(" r%-3d",SR);
            return;
        }
        case Const: {
            printf(" %-4d",SR);
            return;
        }
        case NA: {
            printf(" -   ");
            return;
        }
    }
}

void Operand::prettyPrint() {
    switch (type) {
        case Reg: {
            printf(" r%-3d",SR);
            return;
        }
        case Const: {
            printf(" %-4d",SR);
            return;
        }
        case NA: {
            printf("     ");
            return;
        }
    }
}

void Operand::testPrint() {
    switch (type) {
        case Reg: {
            printf("|r%-5d",SR);
            if (VR == -1) printf("|      ");
            else printf("|%-6d",VR);
            if (PR == -1) printf("|      ");
            else printf("|%-6d",PR);
            if (NU == -1) printf("| INF  ");
            else printf("|%-6d",NU);
            return;
        }
        case Const: {
            printf("|%-6d",SR);
            printf("|      ");
            printf("|      ");
            printf("|      ");
            return;
        }
        case NA: {
            printf("|      ");
            printf("|      ");
            printf("|      ");
            printf("|      ");
        }
    }
}



//      INSTRUCTION FUNCTIONS



void Instruction::prettyPrint() {
    printf("%-7s",getOpCode(opcode));
    OP1.prettyPrint();
    if ((opcode==add)||(opcode==sub)||(opcode==mult)||(opcode==lshift)||(opcode==rshift)) printf(",");
    else printf(" ");
    if (opcode != store) OP2.prettyPrint();
    else OP3.prettyPrint();
    if ((opcode != output) && (opcode != nop)) printf("=> ");
    if (opcode != store) OP3.prettyPrint();
    else OP2.prettyPrint();
    printf("\n");
}

void Instruction::print() {
    printf("| %-7s||",getOpCode(opcode));
    OP1.print();
    printf("|");
    OP2.print();
    printf("|");
    OP3.print();
    printf("|\n");
}

void Instruction::testPrint(int i) {
    printf("| %-5d ",i);
    printf("| %-6s ",getOpCode(opcode));
    OP1.testPrint();
    OP2.testPrint();
    OP3.testPrint();
    printf("|\n");
}



//      BLOCK FUNCTIONS
    


void Block::printPRs() {
    std::list<int>::iterator i;
    printf("| PRs |\n");
    for (i=PRs.begin();i!=PRs.end();i++) {
        printf("| %-4d |\n",*i);
    }
}

void Block::printVRtoPR() {
    int i;
        printf("\n\n-------------------\n");
        printf("|     VRtoPR      |\n");
        printf("-------------------\n");
    for (i=0;i<VRtoPR.size();i++) {
        printf("| %-6d | %-6d |\n",i,VRtoPR[i]);
    }
        printf("-------------------\n\n");
}

void Block::printSRtoVR() {
    int i;
        printf("\n\n-------------------\n");
        printf("|     SRtoVR      |\n");
        printf("-------------------\n");
    for (i=0;i<SRtoVR.size();i++) {
        printf("| %-6d | %-6d |\n",i,SRtoVR[i]);
    }
        printf("-------------------\n\n");
}

void Block::printLastUse() {
    int i;
        printf("\n\n-------------------\n");
        printf("|     LastUse      |\n");
        printf("-------------------\n");
    for (i=0;i<LastUse.size();i++) {
        printf("| %-6d | %-6d |\n",i,LastUse[i]);
    }
        printf("-------------------\n\n");
}

int Block::maxSR() {
    int i;
    int maxReg = -1;
    for (i=0;i<ib.size();i++) {
        if (ib[i].OP1.type == Reg) {
            if (ib[i].OP1.SR>maxReg) {
                maxReg = ib[i].OP1.SR;
            }
        }
        if (ib[i].OP2.type == Reg) {
            if (ib[i].OP2.SR>maxReg) {
                maxReg = ib[i].OP2.SR;
            }
        }
        if (ib[i].OP3.type == Reg) {
            if (ib[i].OP3.SR>maxReg) {
                maxReg = ib[i].OP3.SR;
            }
        }
    }
    return maxReg;
}

int Block::maxVR() {
    int i;
    int maxVR = -1;
    for (i=0;i<ib.size();i++) {
        if (ib[i].OP1.type == Reg) {
            if (ib[i].OP1.VR>maxVR) {
                maxVR = ib[i].OP1.VR;
            }
        }
        if (ib[i].OP2.type == Reg) {
            if (ib[i].OP2.VR>maxVR) {
                maxVR = ib[i].OP2.VR;
            }
        }
        if (ib[i].OP3.type == Reg) {
            if (ib[i].OP3.VR>maxVR) {
                maxVR = ib[i].OP3.VR;
            }
        }
    }
    return maxVR;
}



//      STANDARD PRINT FUNCTIONS



void printToken(Token t) {
    switch(t.category) {
        case INST: {
            printf("<INST, %s>\n",getOpCode(t.opcode));
            return;
        }
        case REG: {
            printf("<REG, r%d>\n",t.value);
            return;
        }
        case CONST: {
            printf("<CONST, %d>\n",t.value);
            return;
        }
        case COMMA: {
            printf("<COMMA, ','>\n");
            return;
        }
        case ARROW: {
            printf("<ARROW, '=>'\n");
            return;
        }
    }
}
        
void printCode(Block b) {
    std::vector<Instruction> block;
    block = b.ib;
    int i;
    for (i=0;i<block.size();i++) {
        block[i].prettyPrint();
    }
}

void printBlock(Block b) {
    std::vector<Instruction> block;
    block = b.ib;
    printf("|index| opcode || op1 | op2 |dest |\n");
    printf("|     |        ||  sr |  sr |  sr |\n");
    int i;
    for(i=0;i!=block.size();i++) {
        printf("|%-5d",i);
        block[i].print();
    }
}

void testPrint(Block b) {
    std::vector<Instruction> block;
    block = b.ib;
    printf("\n\n------------------------------------------------------------------------------------------------------\n");
    printf("| index | opcode |            OP1            |             OP2           |             OP3           |\n");
    printf("|       |        |  SR  |  VR  |  PR  |  NU  |  SR  |  VR  |  PR  |  NU  |  SR  |  VR  |  PR  |  NU  |\n");
    int i = 0;
    for (i=0;i<block.size();i++) {
        block[i].testPrint(i);
    }
    printf("------------------------------------------------------------------------------------------------------\n\n");
    return;
}



//      PRINT HELP FUNCTIONS



void printHelpReader() {
    printf("-------------------------------------------------\n");
    printf("Help file for reader\n");
    printf("-------------------------------------------------\n");
}

void printHelpAllocator() {
    printf("-------------------------------------------------\n");
    printf("Help file for allocator\n");
    printf("-------------------------------------------------\n");
}
