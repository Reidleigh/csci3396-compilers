#include "classes.h"

int getNum(char c);

Token buildToken(tokenCategory cat, opCode op = nop, int val = -1);
Token scanToken(FILE * f);
