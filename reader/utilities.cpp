#include "utilities.h"


bool end(FILE * f) {
    char c = getc(f);
    if (c == EOF)
        return true;
    else {
        ungetc(c,f);
        return false;
    }
}

int getNum(char c) {
    return c-'0';
}

void skipWhiteSpace(FILE * f) {
    bool flag = false;
    char c;
    while (!flag) {
        c = getc(f);
        if (isspace(c)==0) {
            if (c=='/') {
                c = getc(f);
                if (c=='/') {
                    while (c != '\n')
                        c = getc(f);
                } else {
                    printf("c = '%c'\n",c);
                    fprintf(stderr, "Invalid comment!\n"); 
                }
            } else {
                flag = true;
            } 
        }
    }
    ungetc(c,f);
}

void check(bool ex, const char * msg) {
    if (!ex) {
        fprintf(stderr,"%s",msg);
        exit(-1);
    }
}

void test(const char * msg) {
    printf("%s\n",msg);
}
