#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <vector>
#include <list>

bool end(FILE * f);
int getNum(char c);
void skipWhiteSpace(FILE * f);
void check(bool ex, const char * msg);
void test(const char * msg);
