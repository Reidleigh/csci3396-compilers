#include "parser.h"

Instruction parseInst(FILE * f) {
    Token t = scanToken(f);
    Instruction i;
    check((t.category==INST),"First token in parseInst is not an instruction.\n");
    i.opcode = t.opcode;
    switch (t.opcode) {
        case load: {
            i.OP1 = getReg(f); 
            i.OP2 = getNA();
            assertNext(f,ARROW);
            i.OP3 = getReg(f);
            return i;
        }
        case loadI: {
            i.OP1 = getConst(f); 
            i.OP2 = getNA();
            assertNext(f,ARROW);
            i.OP3 = getReg(f);
            return i;
        }
        case store: {
            i.OP1 = getReg(f); 
            assertNext(f,ARROW);
            i.OP2 = getReg(f);
            i.OP3 = getNA();
            return i;
        }
        case output: {
            i.OP1 = getConst(f);
            i.OP2 = getNA();
            i.OP3 = getNA();
            return i;
        }
        case nop: {
            i.OP1 = getNA();
            i.OP2 = getNA();
            i.OP3 = getNA();
            return i;
        }
        default: {
            i.OP1 = getReg(f);
            assertNext(f,COMMA);
            i.OP2 = getReg(f);
            assertNext(f,ARROW);
            i.OP3 = getReg(f);
            return i;
        }
    }
}

Block parseBlock(FILE * f) {
    Block b;
    Instruction i;
    std::vector<Instruction> l;
    while (!end(f)) {
        i = parseInst(f);
        l.push_back(i); 
    }
    b.ib = l;
    return b;
}

Operand newOp() {
    Operand o;
    o.type = NA;
    o.SR = -1;
    o.VR = -1;
    o.PR = -1;
    o.NU = -1;
    return o;
}

void assertNext(FILE * f, tokenCategory cat) {
    Token t = scanToken(f); 
    check((t.category==cat),"Invalid instruction category in parseInst.\n");
}

Operand getReg(FILE * f) {
    Token t;
    Operand o;
    t = scanToken(f);
    check((t.category==REG),"Not REG\n");
    o.type = Reg;
    o.SR = t.value;
    o.VR = -1;
    o.PR = -1;
    o.NU = -1;
    return o;    
}

Operand getConst(FILE * f) {
    Token t;
    Operand o;
    t = scanToken(f);
    check((t.category==CONST),"Not CONST\n");
    o.type = Const;
    o.SR = t.value;
    o.VR = -1;
    o.PR = -1;
    o.NU = -1;
    return o;
}

Operand getNA() {
    Operand o;
    o.type = NA;
    o.SR = -1;
    o.VR = -1;
    o.PR = -1;
    o.NU = -1;
    return o;
}
