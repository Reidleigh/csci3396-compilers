#include "scanner.h"

Instruction parseInst(FILE * f);
Block parseBlock(FILE * f);

Operand newOp();
void assertNext(FILE * f, tokenCategory cat);

Operand getReg(FILE * f);
Operand getConst(FILE * f);
Operand getNA();
