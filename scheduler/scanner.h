#include "utilities.h"

enum tokenCategory {INST, REG, CONST, COMMA, ARROW};
enum opCode {load, loadI, store, add, sub, mult, lshift, rshift, output, nop};
enum opType {Reg, Const, NA};

const char* getTokenCategory(enum tokenCategory cat);
const char* getOpCode(enum opCode op);

class Token {
    public:
    enum tokenCategory category;
    enum opCode opcode;
    int value;
};

class Operand {
    public:
    enum opType type;
    int SR, VR = -1;
};

class Instruction {
    public:
    int label;
    enum opCode opcode;
    class Operand OP1;
    class Operand OP2;
    class Operand OP3;
};

int getNum(char c);

Token buildToken(tokenCategory cat, opCode op = nop, int val = -1);
Token scanToken(FILE * f);

Instruction parseInst(FILE * f);

Operand newOp();
void assertNext(FILE * f, tokenCategory cat);

Operand getReg(FILE * f);
Operand getConst(FILE * f);
Operand getNA();
