#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <vector>
#include <list>
#include <iostream>
#include <map>
#include <algorithm>

void U(std::list<int> * a, std::list<int> b, int x=-1);
bool end(FILE * f);
int getNum(char c);
void skipWhiteSpace(FILE * f);
void check(bool ex, const char * msg);
