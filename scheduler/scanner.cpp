#include "scanner.h"

const char* getTokenCategory(enum tokenCategory cat) {
    switch(cat) {
        case INST: return "INST";
        case REG: return "REG";
        case CONST: return "CONST";
        case COMMA: return "COMMA";
        case ARROW: return "ARROW";
    }
}

const char* getOpCode(enum opCode op) {
    switch(op) {
        case load: return "load";
        case loadI: return "loadI";
        case store: return "store";
        case add: return "add";
        case sub: return "sub";
        case mult: return "mult";
        case lshift: return "lshift";
        case rshift: return "rshift";
        case output: return "output";
        case nop: return "nop";
    }
}


Token buildToken(tokenCategory cat, opCode op, int val) {
    Token nt;
    switch(cat) {
        case INST: nt.opcode = op;
        case REG: nt.value = val;
        case CONST: nt.value = val;
        default:;
    }
    nt.category = cat;
    return nt;
}

Token scanToken(FILE* f) {
    skipWhiteSpace(f);
    char c = getc(f);
    if (c == 's') {
        c = getc(f);
        if (c == 't') {
            if (getc(f) == 'o' && getc(f) == 'r' && getc(f) == 'e') {
                skipWhiteSpace(f);
                return buildToken(INST,store);
            }
        }
        else if (c == 'u') {
            c = getc(f);
            if (c == 'b') {
                skipWhiteSpace(f);
                return buildToken(INST,sub);
            }
        }
    }
    else if (c == 'l') {
        c = getc(f);
        if (c == 'o') {
            c = getc(f);
            if (c == 'a') {
                c = getc(f);
                if (c == 'd') {
                    c = getc(f);
                    if (c == 'I') {
                    skipWhiteSpace(f);
                    return buildToken(INST,loadI);
                    }
                    else {
                    ungetc(c, f);
                    skipWhiteSpace(f);
                    return buildToken(INST,load);
                    }
                }
            }
        }
        else if (c == 's') {
            c = getc(f);
            if (c == 'h') {
                c = getc(f);
                if (c == 'i') {
                    c = getc(f);
                    if (c == 'f') {
                        c = getc(f);
                        if (c == 't') {
                            skipWhiteSpace(f);
                            return buildToken(INST,lshift);
                        }
                    }
                }
            }
        }
    }
    else if (c == 'r') {
        c = getc(f);
        if (c == 's') {
            c = getc(f);
            if (c == 'h') {
                c = getc(f);
                if (c == 'i') {
                    c = getc(f);
                    if (c == 'f') {
                        c = getc(f);
                        if (c == 't') {
                            skipWhiteSpace(f);
                            return buildToken(INST,rshift);
                        }
                    }
                }
            }
        }
        if (isdigit(c)!=0) {
            int num = 0;
            while (isdigit(c)!=0) {
                num = num * 10;
                num = num + getNum(c);
                c =getc(f);
            }
            ungetc(c, f);
            skipWhiteSpace(f);
            return buildToken(REG,nop,num);
        }
    }
    else if (isdigit(c)!=0) {
        int num = 0;
        while (isdigit(c)!=0) {
            num = num * 10;
            num = num + getNum(c);
            c =getc(f);
        }
        ungetc(c,f);
        skipWhiteSpace(f);
        return buildToken(CONST,nop,num);
    }
   else if (c == 'm') {
        c = getc(f);
        if (c == 'u') {
            c = getc(f);
            if (c == 'l') {
                c = getc(f);
                if (c == 't') {
                    skipWhiteSpace(f);
                    return buildToken(INST,mult);
                }
            }
        }
    }
    else if (c == 'a') {
        c = getc(f);
        if (c == 'd') {
            c = getc(f);
            if (c == 'd') {
                skipWhiteSpace(f);
                return buildToken(INST,add);
            }
        }
    }
    else if (c == 'n') {
        c = getc(f);
        if (c == 'o') {
            c = getc(f);
            if (c == 'p') {
                skipWhiteSpace(f);
                return buildToken(INST,nop);
            }
        }
    }
   else if (c == 'o') {
        c = getc(f);
        if (c == 'u') {
            c = getc(f);
            if (c == 't') {
                c = getc(f);
                if (c == 'p') {
                    c = getc(f);
                    if (c == 'u') {
                        c = getc(f);
                        if (c == 't') {
                            skipWhiteSpace(f);
                            return buildToken(INST,output);
                        }
                    }
                }
            }
        }
    }
    else if (c == '=') {
        c = getc(f);
        if (c == '>') {
            skipWhiteSpace(f);
            return buildToken(ARROW);
        }
    }
    else if (c == ',') {
        skipWhiteSpace(f);
        return buildToken(COMMA);
    }
    printf("Error: invalid character\n");
    printf("INVALID CHARACTER: '%c'\n",c);
    exit(-1);
}

Instruction parseInst(FILE * f) {
    Token t = scanToken(f);
    Instruction i;
    check((t.category==INST),"First token in parseInst is not an instruction.\n");
    i.opcode = t.opcode;
    switch (t.opcode) {
        case load: {
            i.OP1 = getReg(f);
            i.OP2 = getNA();
            assertNext(f,ARROW);
            i.OP3 = getReg(f);
            return i;
        }
        case loadI: {
            i.OP1 = getConst(f);
            i.OP2 = getNA();
            assertNext(f,ARROW);
            i.OP3 = getReg(f);
            return i;
        }
        case store: {
            i.OP1 = getReg(f);
            assertNext(f,ARROW);
            i.OP2 = getReg(f);
            i.OP3 = getNA();
            return i;
        }
        case output: {
            i.OP1 = getConst(f);
            i.OP2 = getNA();
            i.OP3 = getNA();
            return i;
        }
        case nop: {
            i.OP1 = getNA();
            i.OP2 = getNA();
            i.OP3 = getNA();
            return i;
        }
        default: {
            i.OP1 = getReg(f);
            assertNext(f,COMMA);
            i.OP2 = getReg(f);
            assertNext(f,ARROW);
            i.OP3 = getReg(f);
            return i;
        }
    }
}

Operand newOp() {
    Operand o;
    o.type = NA;
    o.SR = -1;
    return o;
}

void assertNext(FILE * f, tokenCategory cat) {
    Token t = scanToken(f);
    check((t.category==cat),"Invalid instruction category in parseInst.\n");
}

Operand getReg(FILE * f) {
    Token t;
    Operand o;
    t = scanToken(f);
    check((t.category==REG),"Not REG\n");
    o.type = Reg;
    o.SR = t.value;
    return o;
}

Operand getConst(FILE * f) {
    Token t;
    Operand o;
    t = scanToken(f);
    check((t.category==CONST),"Not CONST\n");
    o.type = Const;
    o.SR = t.value;
    return o;
}

Operand getNA() {
    Operand o;
    o.type = NA;
    o.SR = -1;
    return o;
}
