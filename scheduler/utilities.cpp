#include "utilities.h"

void U(std::list<int> * a, std::list<int> b, int x) {
    if (x==-1) {
        for (auto i=b.begin();i!=b.end();i++) {
            if (std::find(a->begin(),a->end(),*i)==a->end()) a->push_back(*i);
        }
    }
    else {
        for (auto i=b.begin();i!=b.end();i++) {
            if (*i!=x) {
                if (std::find(a->begin(),a->end(),*i)==a->end()) a->push_back(*i);
            }
        }
    }
}

bool end(FILE * f) {
    char c = getc(f);
    if (c == EOF)
        return true;
    else {
        ungetc(c,f);
        return false;
    }
}

int getNum(char c) {
    return c-'0';
}

void skipWhiteSpace(FILE * f) {
    bool flag = false;
    char c;
    while (!flag) {
        c = getc(f);
        if (isspace(c)==0) {
            if (c=='/') {
                c = getc(f);
                if (c=='/') {
                    while (c != '\n')
                        c = getc(f);
                } else {
                    printf("c = '%c'\n",c);
                    fprintf(stderr, "Invalid comment!\n");
                }
            } else {
                flag = true;
            }
        }
    }
    ungetc(c,f);
}

void check(bool ex, const char * msg) {
    if (!ex) {
        fprintf(stderr,"%s\n",msg);
        exit(-1);
    }
}
