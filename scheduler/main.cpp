#include "scheduler.h"

int main(int argc, char * argv[]) {

    FILE * f;
    f = fopen(argv[(argc-1)],"r");
    check((f!=NULL),"Error:Unable to open file.\n");

    for (int i=1;i<argc;i++) {
        if (strcmp(argv[i],"-h")==0) {
            printHelp();
            return 0;
        }
    }

    Block b;
    b.parseBlock(f);

    return 0;
}
