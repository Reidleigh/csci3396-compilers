#include "scheduler.h"

void Block::parseBlock(FILE * f) {
    check(f!=NULL,"Error: cannot parse block because input file is NULL");

    while (!end(f)) { 
        Instruction i = parseInst(f);
        i.label = size;
        block[size++] = i;
        maxReg = std::max(maxReg,i.OP1.SR);
        maxReg = std::max(maxReg,i.OP2.SR);
        maxReg = std::max(maxReg,i.OP3.SR);
    }

    calculateVR();
    makeDependenceGraph();
    calculateWeights();
    print();
}

void Block::calculateVR() {
    SRtoVR.resize(maxReg+1, -1);

    for (int i=size-1;i>0;i--) {
        if (block[i].OP3.type==Reg) {
            Update(&(block[i].OP3),i);
            SRtoVR[block[i].OP3.SR] = -1;
        }
        if (block[i].OP1.type==Reg) Update(&(block[i].OP1),i);
        if (block[i].OP2.type==Reg) Update(&(block[i].OP2),i);
    }
}

void Block::Update(Operand * op, int i) {
    if (SRtoVR[op->SR] == -1) {
        SRtoVR[op->SR] = VRName++;
    }
    op->VR = SRtoVR[op->SR];
}

void Block::makeDependenceGraph() {
    //Error Checking
    check(!block.empty(),"Error: cannot schedule block because it is empty");
    
    // Storage for Scheduling
    int store = -1;
    int output = -1;
    std::list<int> loads; // Stores all load instructions past the most recent store
    std::map<int,int> def; // Stores <reg,label>, means reg is defined at that label
    for (int i=0; i<maxReg; i++) {
        def[i] = -1;
    }
    
    
    // Main Loop to Calculate Dependence Graph
    for (int i=1; i<size; i++) {
        std::list<int> temp;
        // Calculate Register Edges
        if (block[i].opcode != nop && block[i].opcode != output && block[i].opcode != loadI) {
            check(def[block[i].OP1.VR]>=0,"Error: Undefined Register");
            temp.push_back(def[block[i].OP1.VR]);
            if (block[i].opcode != load) {
                check(def[block[i].OP2.VR]>=0,"Error: Undefined Register");
                temp.push_back(def[block[i].OP2.VR]);
            }
        }
        // Handle Storage and Serialization Edges
        if (block[i].opcode == 2) {             // Store
            if (store != -1) temp.push_back(store);
            if (output != -1) temp.push_back(output);
            U(&edges[i],loads);
            store = i;
            while (!loads.empty()) loads.pop_back();
        } else if (block[i].opcode == 8) {     // Output
            if (output != -1) temp.push_back(output);
            if (store != -1) temp.push_back(store);
            output = i;
        } else if (block[i].opcode == load) {       // Load
            if (store != -1) temp.push_back(store);
            loads.push_back(i);
            def[block[i].OP3.VR] = i;
        } else if (block[i].opcode == loadI) {      // LoadI
            def[block[i].OP3.VR] = i;
        } else if (block[i].opcode == nop) {        // Nop
            // Do nothing
        } else {                                    // Add, sub, mult, lshift, rshift
            def[block[i].OP3.VR] = i;
        }
        U(&edges[i],temp);
    }
    for (int i=1;i<size;i++) {
        for (auto it=edges[i].begin();it!=edges[i].end();it++) {
            if (std::find(redges[*it].begin(),redges[*it].end(),i)==redges[*it].end())
                redges[*it].push_back(i);
        }
    }
}

void Block::calculateWeights() {
    std::list<int> roots;
    std::list<int> rroots;
    for (int i=1;i<size;i++) {
        weights[i] = -1;
    }
    // calculates roots
    for (int i=1;i<size;i++) {
        if (redges[i].empty()) {
            roots.push_back(i);
            weights[i] = lat(block[i].opcode);
        }
        if (edges[i].empty()) {
            rroots.push_back(i);
        }
    }

    for (auto i=rroots.begin();i!=rroots.end();i++) {
        calcWeightsHelper(*i);
    }
}

int Block::calcWeightsHelper(int i) {
    if (weights[i]!=-1) {
        return weights[i];
    } else {
        int laten = lat(block[i].opcode);
        int weight = 0;
        for (auto it=redges[i].begin();it!=redges[i].end();it++) {
            weight = std::max(weight,(laten + calcWeightsHelper(*it)));
        }
        weights[i] = weight;
        return weight;
    }
}

int Block::lat(opCode o) {
    switch (o) {
        case load: return 3;
        case store: return 3;
        case mult: return 2;
        default: return 1;
    }
}

void Block::print() {
    printf("nodes:\n");
    for (int i=1;i<size;i++) {
        std::cout << "     n" << block[i].label << " : ";
        std::cout << getOpCode(block[i].opcode) << " ";
        if (block[i].opcode == store) {
            std::cout << "r" << block[i].OP1.VR << " => r";
            std::cout << block[i].OP2.VR << std::endl;
        } else { 
            if (block[i].opcode != loadI) 
                std::cout << "r";
            if (block[i].opcode == loadI)
                std::cout << block[i].OP1.SR;
            else
                std::cout << block[i].OP1.VR;
            if (block[i].OP2.VR!=-1)
                std::cout << ", r" << block[i].OP2.VR;
            std::cout << " => r" << block[i].OP3.VR << std::endl;
        }
    }
    printf("\n");
    printEdges();
    printf("\n");
    printWeights();
}

void Block::printEdges() {
    std::cout << "edges:" << std::endl;
    for (int i=1;i<size;i++) {
        std::cout << "     n" << i << " : { ";
        if (!edges[i].empty()) {
            auto it=edges[i].begin();
            std::cout << "n" << *it;
            for (it++;it!=edges[i].end();it++) {
                std::cout << ", n" << *it;
            }
        }
            std::cout << " }" << std::endl;
    }
}

void Block::printWeights() {
    std::cout << "weights:" << std::endl;
    for (int i=1;i<size;i++) {
        std::cout << "     n" << i << " : " << weights[i] << std::endl;
    }
}

void printHelp() {
    std::cout << "Usage: sched [options] filename" << std::endl;
    std::cout << "-h    prints a list of valid command-line arguments and quits." << std::endl; 
}
