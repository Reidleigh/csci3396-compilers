#include "scanner.h"

class Block {
    public:
    int size = 1;
    int maxReg = 0;
    std::map<int,Instruction> block;
    std::map<int,std::list<int>> edges; // notes that i is dependent on
    std::map<int,std::list<int>> redges; // notes that depend on i
    std::map<int,int> weights;

    int lat(opCode o);
    int calcWeightsHelper(int i);
    int VRName = 0;
    std::vector<int> SRtoVR;
    void calculateVR();
    void Update(Operand * op, int i);

    void parseBlock(FILE * f);
    void makeDependenceGraph();
    void calculateWeights();
    void print();
    void printEdges();
    void printWeights();
};

void printHelp();
