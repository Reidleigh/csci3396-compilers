#include "block.h"

void Block::allocateRegisters(int k) {
    int rx,ry,rz;
    maxPR = k-1;
    memAddress = 32768;
    for (int j=0;j<(k-1);j++) {
        PRs.push_back(j);
    }
    
    VRtoMEM.resize(VRName,-1);
    VRtoPR.resize(VRName,-1);
    PRtoVR.resize(maxPR, -1);
    PRtoNU.resize(maxPR,-1);

    if(verbose) printTables();
    for(auto i = ir.begin(); i != ir.end(); i++) {
        rx = ensure(&(i->OP1),i, 1);
        ry = ensure(&(i->OP2),i, 2);
        free(&(i->OP1));
        free(&(i->OP2));
        rz = allocate(&(i->OP3),i, 3);
        free(&(i->OP3));
        if(verbose) {
            i->testPrint();
            printTables();
        }
        
    }

}

void Block::free(Operand * op) {
    if (op->type==Reg) {
        int pr = op->PR;
        if  ((pr != -1) && (op->NU == -1)) {
            PRs.push_front(pr);
            PRtoNU[pr] = -1;
            VRtoPR[op->VR]=-1;
            PRtoVR[pr] = -1;
        } else {
            PRtoNU[pr] = op->NU;
        }
    }
}

int Block::ensure(Operand * op, std::list<Instruction>::iterator i, int on) {
    if (op->type==Reg) {
        if (VRtoPR[op->VR]==-1) {
            allocate(op, i, on);
            Load(op, i);
        } else {
            op->PR = VRtoPR[op->VR];
            PRtoVR[op->PR] = op->VR;
        }
        return op->PR;
    }
    else return -1;
}

void Block::Load(Operand * op, std::list<Instruction>::iterator i) {
    //x is the PR that we want to load the VR of op into
    //first, let's create the instruction
    Instruction inA = newInstruction();
    Instruction inB = newInstruction();

    inA.opcode = loadI;
    inA.OP1.type = Const;
    inA.OP1.SR = VRtoMEM[op->VR];
    inA.OP3.type = Reg;
    inA.OP3.PR = maxPR;

    inB.opcode = load;
    inB.OP1.type = Reg;
    inB.OP1.PR = maxPR;
    inB.OP3.type = Reg;
    inB.OP3.PR = op->PR;
    inB.OP3.VR = op->VR;

    //then, let's update the tables
    PRtoVR[op->PR] = op->VR;
    VRtoPR[op->VR] = op->PR;
    PRtoNU[op->PR] = -1;
    VRtoMEM[op->VR] = -1;

    //now, let's insert the new instructions into the intermediate rep
    ir.insert(i,inA);
    ir.insert(i,inB);
    if(verbose) {
        inA.testPrint();
        inB.testPrint();
    }
        
}

int Block::allocate(Operand * op, std::list<Instruction>::iterator i, int on) {
    if (op->type == Reg) {
       int x;
       if (!PRs.empty()) {
           x = PRs.front();
           PRs.pop_front();
           op->PR = x;
           PRtoVR[x] = op->VR;
           PRtoNU[x] = op->NU;
           VRtoPR[op->VR] = x;
           return x;
       } else {
           int j,k;
           k = -1;
           int maximal = -1;
           for (j=0;j<maxPR;j++) {
               if (PRtoNU[j] > maximal) {
                   maximal = PRtoNU[j];
                   k=j;
               }
           }
           Store(k,i);
           op->PR = k;
           PRtoVR[k] = op->VR;
           PRtoNU[k] = -5; 
           VRtoPR[op->VR] = k;
           return k;
       }
   }
   else return -1;
}

void Block::Store(int k, std::list<Instruction>::iterator i) {
    Instruction inA = newInstruction();
    Instruction inB = newInstruction();
    if(k < 0) {
        printf("AAAAH!\n");
        //exit(0);
    }

    inA.opcode = store;
    inA.OP1.type = Reg;
    inA.OP1.PR = k;
    inA.OP1.VR = PRtoVR[k];
    inA.OP2.type = Reg;
    inA.OP2.PR = maxPR;

    inB.opcode = loadI;
    inB.OP1.type = Const;
    inB.OP1.SR = memAddress;
    inB.OP3.type = Reg;
    inB.OP3.PR = maxPR;
    if(verbose) {
        inB.testPrint();
        inA.testPrint();
    }

    VRtoPR[PRtoVR[k]] = -1;
    VRtoMEM[PRtoVR[k]] = memAddress;
    memAddress = memAddress + 4;
    ir.insert(i,inB);
    ir.insert(i,inA);  
}



void Block::computeLastUse() {
    int n, i;
    maxSR();
    n = ir.size();
    VRName = 0;

    SRtoVR.resize(maxReg+1, -1);
    LastUse.resize(maxReg+1, -1);
    i = n-1;
    for (auto iter = ir.rbegin(); iter != ir.rend(); ++iter) {
    //for (i=n-1;i>=0;i--) {
        if (iter->OP3.type==Reg) {
            Update(&(iter->OP3),i);
            SRtoVR[iter->OP3.SR] = -1;
            LastUse[iter->OP3.SR] = -1;
        }
        if (iter->OP1.type==Reg) Update(&(iter->OP1),i);
        if (iter->OP2.type==Reg) Update(&(iter->OP2),i);
        i--;
    }
}

void Block::Update(Operand * op, int i) {
    if (SRtoVR[op->SR] == -1) {
        SRtoVR[op->SR] = VRName++;
    }
    op->VR = SRtoVR[op->SR];
    op->NU = LastUse[op->SR];
    LastUse[op->SR] = i;
}

void Block::printTables() {
   printVRtoPR();
   printVRtoMEM();
   printPRtoVR();
   printStack();
    
}

void Block::printStack() {
    std::list<int>::iterator i;
    printf(">Stack: ");
    for (i=PRs.begin();i!=PRs.end();i++) {
        printf("%d,",*i);
    }
    printf("\n");
}

void Block::printPRtoVR() {
    int i;
        printf(">PRtoVR (NU)\n");
    for (i=0;i<PRtoVR.size();i++) {
         printf(">>%d: %d (%d)\n",i,PRtoVR[i], PRtoNU[i]);
    }
}

void Block::printVRtoPR() {
    int i;
        printf(">VRtoPR\n");
    for (i=0;i<VRtoPR.size();i++) {
        if(VRtoPR[i] != -1) {
            printf(">>%d: %d\n",i,VRtoPR[i]);
        }
    }
}

void Block::printVRtoMEM() {
    int i;
        printf(">VRtoMEM\n");
    for (i=0;i<VRtoMEM.size();i++) {
        if(VRtoMEM[i] != -1) {
            printf(">>%d: %d\n",i,VRtoMEM[i]);
        }
    }
}

void Block::printSRtoVR() {
    int i;
        printf("\n\n-------------------\n");
        printf("|     SRtoVR      |\n");
        printf("-------------------\n");
    for (i=0;i<SRtoVR.size();i++) {
        printf("| %-6d | %-6d |\n",i,SRtoVR[i]);
    }
        printf("-------------------\n\n");
}

void Block::printLastUse() {
    int i;
        printf("\n\n-------------------\n");
        printf("|     LastUse      |\n");
        printf("-------------------\n");
    for (i=0;i<LastUse.size();i++) {
        printf("| %-6d | %-6d |\n",i,LastUse[i]);
    }
        printf("-------------------\n\n");
}

void Block::maxSR() {
    maxReg = -1;
    for(auto iter = ir.begin(); iter != ir.end(); iter++) {
        if (iter->OP1.type == Reg) {
            if (iter->OP1.SR>maxReg) {
                maxReg = iter->OP1.SR;
            }
        }
        if (iter->OP2.type == Reg) {
            if (iter->OP2.SR>maxReg) {
                maxReg = iter->OP2.SR;
            }
        }
        if (iter->OP3.type == Reg) {
            if (iter->OP3.SR>maxReg) {
                maxReg = iter->OP3.SR;
            }
        }
    }
}

void Block::printCode () {
    int i;
    for(auto iter = ir.begin(); iter != ir.end(); iter++) {
        iter->prettyPrint();
    }
}

void Block::printBlock() {
    printf("|index| opcode || op1 | op2 |dest |\n");
    printf("|     |        ||  sr |  sr |  sr |\n");
    for(auto iter = ir.begin(); iter != ir.end(); iter++) {
        //printf("|%-5d",i);
        iter->print();
    }
}

void Block::testPrint() {
    printf("\n\n------------------------------------------------------------------------------------------------------\n");
    printf("| index | opcode |            OP1            |             OP2           |             OP3           |\n");
    printf("|       |        |  SR  |  VR  |  PR  |  NU  |  SR  |  VR  |  PR  |  NU  |  SR  |  VR  |  PR  |  NU  |\n");
    int i = 0;
    for(auto iter = ir.begin(); iter != ir.end(); iter++) {
        iter->testPrint();
    }
    printf("------------------------------------------------------------------------------------------------------\n\n");
    return;
}

Operand newOperand() { 
    Operand o;
    o.type = NA;
    o.SR = -1;
    o.VR = -1;
    o.PR = -1;
    o.NU = -1;
    return o;
}

Instruction newInstruction() {
    Instruction i;
    i.opcode = nop;
    i.index = -1;
    i.OP1 = newOperand();
    i.OP2 = newOperand();
    i.OP3 = newOperand();
    return i;
}
