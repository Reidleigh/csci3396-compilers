#include "classes.h"

class Block {
    public:
    int VRName, maxReg, memAddress;
    int maxPR;
    bool verbose;
    std::list<Instruction> ir;
    std::list<int> PRs;             //initialized in allocateRegisters
    std::vector<int> SRtoVR;        //initialized in computeLastUse
    std::vector<int> PRtoVR;        //initialized in allocateRegisters
    std::vector<int> LastUse;       //initialized in computeLastUse
    std::vector<int> VRtoPR;        //initialized in allocateRegisters
    std::vector<int> PRtoNU;        //initialized in allocateRegisters
    std::vector<int> VRtoMEM;       //initialized in allocateRegisters

    void allocateRegisters(int k);
    void free(Operand * op);
    int ensure(Operand * op, std::list<Instruction>::iterator i, int on); 
    void Load(Operand * op, std::list<Instruction>::iterator i);
    int allocate(Operand * op, std::list<Instruction>::iterator i, int on);
    void Store(int k, std::list<Instruction>::iterator i);
    void computeLastUse();
    void Update(Operand * op, int i);

    void maxSR();
    void printStack();
    void printTables();
    void printSRtoVR();
    void printLastUse();
    void printVRtoPR();
    void printVRtoMEM();
    void printPRtoVR();
    void printBlock();
    void printCode();
    void testPrint();
};

Operand newOperand();
Instruction newInstruction();


