#include "scanner.h"

Token buildToken(tokenCategory cat, opCode op, int val) {
    Token nt;
    switch(cat) {
        case INST: nt.opcode = op;
        case REG: nt.value = val;
        case CONST: nt.value = val;
        default:;
    }
    nt.category = cat; 
    return nt;
}

Token scanToken(FILE* f) {
    skipWhiteSpace(f);
    char c = getc(f);
    if (c == 's') {
        c = getc(f);
        if (c == 't') {
            if (getc(f) == 'o' && getc(f) == 'r' && getc(f) == 'e') {
                skipWhiteSpace(f);
                return buildToken(INST,store);
            }
        }
        else if (c == 'u') {
            c = getc(f);
            if (c == 'b') {
                skipWhiteSpace(f);
                return buildToken(INST,sub);
            }
        }
    }
    else if (c == 'l') {
        c = getc(f);
        if (c == 'o') {
            c = getc(f);
            if (c == 'a') {
                c = getc(f);
                if (c == 'd') {
                    c = getc(f);
                    if (c == 'I') {
                    skipWhiteSpace(f);
                    return buildToken(INST,loadI);
                    }
                    else {
                    ungetc(c, f);
                    skipWhiteSpace(f);
                    return buildToken(INST,load);
                    }
                }
            }
        }
        else if (c == 's') {
            c = getc(f);
            if (c == 'h') {
                c = getc(f);
                if (c == 'i') {
                    c = getc(f);
                    if (c == 'f') {
                        c = getc(f);
                        if (c == 't') {
                            skipWhiteSpace(f);
                            return buildToken(INST,lshift);
                        }
                    }
                }
            }
        }
    }
    else if (c == 'r') {
        c = getc(f);
        if (c == 's') {
            c = getc(f);
            if (c == 'h') {
                c = getc(f);
                if (c == 'i') {
                    c = getc(f);
                    if (c == 'f') {
                        c = getc(f);
                        if (c == 't') {
                            skipWhiteSpace(f);
                            return buildToken(INST,rshift);
                        }
                    }
                }
            }
        }
        if (isdigit(c)!=0) {
            int num = 0;
            while (isdigit(c)!=0) {
                num = num * 10;
                num = num + getNum(c);     
                c =getc(f);
            }
            ungetc(c, f);
            skipWhiteSpace(f);
            return buildToken(REG,nop,num);
        } 
    }
    else if (isdigit(c)!=0) {
        int num = 0;
        while (isdigit(c)!=0) {
            num = num * 10;
            num = num + getNum(c);     
            c =getc(f);
        }
        ungetc(c,f);
        skipWhiteSpace(f);
        return buildToken(CONST,nop,num);
    }
    else if (c == 'm') {
        c = getc(f);
        if (c == 'u') {
            c = getc(f);
            if (c == 'l') {
                c = getc(f);
                if (c == 't') {
                    skipWhiteSpace(f);
                    return buildToken(INST,mult);
                }
            }
        }
    }
    else if (c == 'a') {
        c = getc(f);
        if (c == 'd') {
            c = getc(f);
            if (c == 'd') {
                skipWhiteSpace(f);
                return buildToken(INST,add);
            }
        }
    }
    else if (c == 'n') {
        c = getc(f);
        if (c == 'o') {
            c = getc(f);
            if (c == 'p') {
                skipWhiteSpace(f);
                return buildToken(INST,nop);
            }
        }
    }
    else if (c == 'o') {
        c = getc(f);
        if (c == 'u') {
            c = getc(f);
            if (c == 't') {
                c = getc(f);
                if (c == 'p') {
                    c = getc(f);
                    if (c == 'u') {
                        c = getc(f);
                        if (c == 't') {
                            skipWhiteSpace(f);
                            return buildToken(INST,output);
                        }
                    }
                }
            }
        }
    }
    else if (c == '=') {
        c = getc(f);
        if (c == '>') {
            skipWhiteSpace(f);
            return buildToken(ARROW);
        }
    }
    else if (c == ',') {
        skipWhiteSpace(f);
        return buildToken(COMMA);
    }
    printf("Error: invalid character\n");
    printf("INVALID CHARACTER: '%c'\n",c);
    exit(-1);
}
