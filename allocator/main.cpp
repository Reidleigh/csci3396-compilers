#include "parser.h"
#include <string>


int main(int argc, char* argv[]) {

    bool verbose = false;
    check((argc>1),"Error: no file argument\n");
    int k = 5;
    for (int i=1;i<argc;i++) {

    if (strcmp(argv[i],"-h")==0) {
        printHelpAllocator();
        return 0;
    }
    if (strcmp(argv[i],"-v")==0) {
        verbose = true;
    }
    if (strcmp(argv[i],"-k")==0) {
        k = std::stoi(argv[i+1]);
    } 
    }
    

    FILE * f;
    f = fopen(argv[argc-1],"r");
    check((f!=NULL),"Error: unable to open file\n");

    Block b = parseBlock(f); 
    b.verbose = verbose;
    //printf("\n");
    //b.printBlock();

    b.computeLastUse();
    //b.testPrint();

    b.allocateRegisters(k);
    if(verbose) {
        b.testPrint();
    }
    b.printCode();

    return 0;

}    
