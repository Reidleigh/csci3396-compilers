#include "classes.h"



//      OPERAND FUNCTIONS



const char* getTokenCategory(enum tokenCategory cat) {
    switch(cat) {
        case INST: return "INST";
        case REG: return "REG";
        case CONST: return "CONST";
        case COMMA: return "COMMA";
        case ARROW: return "ARROW";
    }
}

const char* getOpCode(enum opCode op) {
    switch(op) {
        case load: return "load";
        case loadI: return "loadI";
        case store: return "store";
        case add: return "add";
        case sub: return "sub";
        case mult: return "mult";
        case lshift: return "lshift";
        case rshift: return "rshift";
        case output: return "output";
        case nop: return "nop";
    }
} 

void Operand::print() {
    switch (type) {
        case Reg: {
            printf(" r%-3d",SR);
            return;
        }
        case Const: {
            printf(" %-4d",SR);
            return;
        }
        case NA: {
            printf(" -   ");
            return;
        }
    }
}

void Operand::prettyPrint() {
    switch (type) {
        case Reg: {
            printf(" r%-3d",PR);
            return;
        }
        case Const: {
            printf(" %-4d",SR);
            return;
        }
        case NA: {
            printf("     ");
            return;
        }
    }
}

void Operand::testPrint() {
    switch (type) {
        case Reg: {
            printf("|r%-5d",SR);
            if (VR == -1) printf("|      ");
            else printf("|%-6d",VR);
            if (PR == -1) printf("|      ");
            else printf("|%-6d",PR);
            if (NU == -1) printf("| INF  ");
            else printf("|%-6d",NU);
            return;
        }
        case Const: {
            printf("|%-6d",SR);
            printf("|      ");
            printf("|      ");
            printf("|      ");
            return;
        }
        case NA: {
            printf("|      ");
            printf("|      ");
            printf("|      ");
            printf("|      ");
        }
    }
}



//      INSTRUCTION FUNCTIONS



void Instruction::prettyPrint() {
    printf("%-7s",getOpCode(opcode));
    OP1.prettyPrint();
    if ((opcode==add)||(opcode==sub)||(opcode==mult)||(opcode==lshift)||(opcode==rshift)) printf(",");
    else printf(" ");
    if (opcode != store) OP2.prettyPrint();
    else OP3.prettyPrint();
    if ((opcode != output) && (opcode != nop)) printf("=> ");
    if (opcode != store) OP3.prettyPrint();
    else OP2.prettyPrint();
    printf("\n");
}

void Instruction::print() {
    printf("| %-5d ",index);
    printf("| %-7s||",getOpCode(opcode));
    OP1.print();
    printf("|");
    OP2.print();
    printf("|");
    OP3.print();
    printf("|\n");
}

void Instruction::testPrint() {
    printf("|%-3d",index);
    printf("|%-4s",getOpCode(opcode));
    OP1.testPrint();
    OP2.testPrint();
    OP3.testPrint();
    printf("|\n");
}



//      STANDARD PRINT FUNCTIONS



void printToken(Token t) {
    switch(t.category) {
        case INST: {
            printf("<INST, %s>\n",getOpCode(t.opcode));
            return;
        }
        case REG: {
            printf("<REG, r%d>\n",t.value);
            return;
        }
        case CONST: {
            printf("<CONST, %d>\n",t.value);
            return;
        }
        case COMMA: {
            printf("<COMMA, ','>\n");
            return;
        }
        case ARROW: {
            printf("<ARROW, '=>'\n");
            return;
        }
    }
}



//      PRINT HELP FUNCTIONS



void printHelpReader() {
    printf("-------------------------------------------------\n");
    printf("Help file for reader\n");
    printf("-------------------------------------------------\n");
}

void printHelpAllocator() {
    printf("-------------------------------------------------\n");
    printf("Help file for allocator\n");
    printf("-------------------------------------------------\n");
}
